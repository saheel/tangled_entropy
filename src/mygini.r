mygini <- function(bcnts, tlocs) {
    bdens = bcnts/tlocs
    o = rev(order(bdens))
    obcnts = bcnts[o]
    otlocs = tlocs[o]

    ocbcnts = cumsum(as.numeric(obcnts))
    ocbcnts_percents = ocbcnts/ocbcnts[length(ocbcnts)]
    octlocs = cumsum(as.numeric(otlocs))

    polyarea = 0.0
    triarea = 0.5
    heights = append(c(0), ocbcnts_percents)
    widths = otlocs/octlocs[length(octlocs)]
    n = length(widths)
    ## print(widths)
    ## print(heights)
    for (i in 1:n) {
        polyarea = polyarea + 0.5*widths[i]*(heights[i] + heights[i+1])
    }

    return(2*(polyarea-triarea))
}

project_names = c('twemproxy', 'beanstalkd', 'the_silver_searcher', 'skynet', 'bitcoin', 'ccv', 'mruby', 'libuv', 'libgit2', 'tig', 'OpenSubdiv', 'cjdns', 'numpy', 'macvim', 'redis', 'cocos2d-x', 'opencv', 'php-src', 'xbmc')
for (project_name in project_names) {
    cat(project_name, '\n')
    d1 = read.csv(paste('data/ast_node_instances/', project_name, '.file.bugdata.csv', sep=''))
    cat(mygini(d1$bcnt, d1$avg_loc), '\n')
}

## nt = c('for', 'while', 'do')
## csv_data = read.csv('data/ast_node_instances/php-src.bugdata.csv')
## csv_data_nt_only = csv_data[which((csv_data$node_type %in% nt) & csv_data$num_overall_bugs>0, ), c('avg_loc', 'num_overall_bugs')]
## csv_data_nt_only = data.frame(num_overall_bugs = as.numeric(csv_data_nt_only$num_overall_bugs), avg_loc = as.numeric(as.integer(csv_data_nt_only$avg_loc)))
## mygini(csv_data_nt_only$num_overall_bugs, csv_data_nt_only$avg_loc)
