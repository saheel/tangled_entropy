import os, sys
import pandas as pd
import numpy as np

if __name__ == '__main__':
    projects = ['atmosphere',  'derby',  'elasticsearch',  'facebook-android-sdk',  
                'lucene',  'netty',  'openjpa',  'presto',  'qpid', 'wicket']

    df_out_all = pd.DataFrame()
    if os.path.isdir('summarized_data'):
        os.mkdir('summarized_data')
    for project in projects:
        print('Working on ' + project + '...')
        csv_fpath = os.path.join('/home/saheel/tangled_entropy/filtered_data/', project + '.csv')
        # csv_fpath = os.path.join('/home/bairay/bitbucket/szz/R/data/entropies_1month', project + '.csv')

        df = pd.read_csv(csv_fpath, 
                         usecols = ['project', 'snapshot', 'filename', 'entropy', 
                                    'is_bug', 'bf_sha', 'bf_add', 'bf_del'], 
                         index_col=False)
        df_buggy = df[df.is_bug == 1]

        bf_shas = df_buggy.bf_sha.unique()
        bf_sha_sizes = []     # sizes of bf_shas
        num_50 = []           # percent of high entropy lines in bf_shas that constitute for 50% of total entropy
        num_60 = []           # percent of high entropy lines in bf_shas that constitute for 60% of total entropy
        num_70 = []           # percent of high entropy lines in bf_shas that constitute for 70% of total entropy
        num_80 = []           # percent of high entropy lines in bf_shas that constitute for 80% of total entropy
        for bf_sha in bf_shas:
            # TODO Verify bf_sha_size by an independent method, e.g., look at the commit itself
            filenames = df_buggy[df_buggy.bf_sha == bf_sha].filename.unique()
            bf_adds = [df_buggy[(df_buggy.bf_sha == bf_sha) & (df_buggy.filename == f)].bf_add.unique()[0] for f in filenames]
            bf_dels = [df_buggy[(df_buggy.bf_sha == bf_sha) & (df_buggy.filename == f)].bf_del.unique()[0] for f in filenames]
            
            bf_sha_size = sum(bf_adds) + sum(bf_dels)
            bf_sha_sizes.append(bf_sha_size)

            # # This should also work with minor modifications.
            # bf_add_total = df_buggy[df_buggy.bf_sha == bf_sha].groupby(['filename', 'bf_sha'])['bf_add'].unique().sum()[0]
            # bf_del_total = df_buggy[df_buggy.bf_sha == bf_sha].groupby(['filename', 'bf_sha'])['bf_del'].unique().sum()[0]
            # bf_sha_sizes.append(bf_add_total + bf_del_total)
        
            entropies = df_buggy[df_buggy.bf_sha == bf_sha].entropy.values
            entropies_rev_sorted = sorted(entropies, reverse=True)
            entropies_cumsum = np.cumsum(entropies_rev_sorted)
            total_entropy_sum = entropies_cumsum[-1]

            for index, entropy_sum in enumerate(entropies_cumsum):
                if 1.0*entropy_sum/total_entropy_sum > 0.5:
                    break
            num_50.append(100.0*(index + 1)/len(entropies_cumsum))

            for index, entropy_sum in enumerate(entropies_cumsum):
                if 1.0*entropy_sum/total_entropy_sum > 0.6:
                    break
            num_60.append(100.0*(index + 1)/len(entropies_cumsum))

            for index, entropy_sum in enumerate(entropies_cumsum):
                if 1.0*entropy_sum/total_entropy_sum > 0.7:
                    break
            num_70.append(100.0*(index + 1)/len(entropies_cumsum))

            for index, entropy_sum in enumerate(entropies_cumsum):
                if 1.0*entropy_sum/total_entropy_sum > 0.8:
                    break
            num_80.append(100.0*(index + 1)/len(entropies_cumsum))

        df_out = pd.DataFrame({'project': project,
                               'size': bf_sha_sizes,
                               'num_50': num_50,
                               'num_60': num_60,
                               'num_70': num_70,
                               'num_80': num_80},
                              columns = ['project', 'size', 'num_50', 'num_60', 'num_70', 'num_80'])
        df_out.to_csv('summarized_data/' + project + '_summary.csv', index=False)
        df_out_all = df_out_all.append(df_out)

    df_out_all.to_csv('summarized_data/all_summary.csv', index=False)
