- Get gini coefficient for each bf commit
- Get hex-bin plot for gini v/s size of bf commit

- DONE repeat plots at 40%, 50%, etc.

- check the entropy of buggy lines v/s surrounding N lines
- hypothesis: had a developer committed the surrounding lines as part of a tangled bf commit, you would be able to untangle the bf commit.
